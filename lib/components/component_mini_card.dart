import 'package:flutter/material.dart';

class ComponentMiniCard extends StatelessWidget {
  const ComponentMiniCard(
      {super.key, required this.imageName, required this.imageText});

  final String imageName;
  final String imageText;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          SizedBox(
            height: 30,
          ),
          SizedBox(
            width: 100,
            height: 100,
            child: Image.asset(
              'assets/${imageName}',
              fit: BoxFit.fill,
            ),
          ),
          const SizedBox(
            height: 30,
          ),
          Container(
            child: Text(imageText),
          ),
        ],
      ),
    );
  }
}
