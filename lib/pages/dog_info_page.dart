import 'package:flutter/material.dart';

class DogInfoPage extends StatelessWidget {
  const DogInfoPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('귀여운 강아지 소개'), // 한번 넣으면 변하지 않을 거니까(강제로 텍스트로 넣어뒀으니) const화
      ),
      body: Column(
        children: [
          const SizedBox(
            height: 20,
          ),
          SizedBox(
            width: MediaQuery.of(context).size.width,
            height: 200,
            child: Image.asset('assets/dog.png'),
          ),
          const SizedBox(
            height: 30,
          ),
          const Center(
              child: Text('귀여운 강아지'),
            ), //flutter에서는 모든 마감을 ,로 하길 권장 = 위치 변경할 일이 많
        ],
      ),
    );
  }
}
