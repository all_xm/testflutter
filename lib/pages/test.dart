import 'package:flutter/material.dart';

class Test extends StatefulWidget {
  const Test({Key? key}) : super(key: key);

  @override
  State<Test> createState() => _TestState();
}

class _TestState extends State<Test> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          const SizedBox(
            height: 10,
          ),
          Stack(
            children: [
              SizedBox(
                width: MediaQuery.of(context).size.width,
                height: 350,
                child: Image.asset(
                  'assets/flour.jpg',
                  fit: BoxFit.fill,
                ),
              ),
              Positioned(
                top: 100,
                left: MediaQuery.of(context).size.width / 2 - 92,
                child: SizedBox(
                  width: 120,
                  height: 120,
                  child: Image.asset('assets/dog.png'),
                ),
              ),
              Positioned(
                top: 290,
                left: MediaQuery.of(context).size.width / 2 - 65,
                child: ElevatedButton(
                  onPressed: () {},
                  child: const Text("밀가루 모으기"),
                ),
              ),
            ],
          ),
          const SizedBox(
            height: 10,
          ),
          Container(
            width: MediaQuery.of(context).size.width,
            height: 30,
            color: Colors.white,
            child: const Center(
              child: Text(
                '재료 00개',
                style: TextStyle(fontSize: 15),
              ),
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Row(
                children: [
                  SizedBox(
                    width: MediaQuery.of(context).size.width / 4,
                    height: 100,
                    child: Image.asset('assets/dog.png'),
                  ),
                  SizedBox(
                    child: ElevatedButton(
                      onPressed: () {},
                      child: const Text('뽑기'),
                    ),
                  ),
                ],
              ),
              Row(
                children: [
                  SizedBox(
                    width: MediaQuery.of(context).size.width / 4,
                    height: 100,
                    child: Image.asset('assets/dog.png'),
                  ),
                  SizedBox(
                    child: ElevatedButton(
                      onPressed: () {},
                      child: const Text('뽑기'),
                    ),
                  ),
                ],
              ),
            ],
          ),
          const SizedBox(
            height: 10,
          ),
          SizedBox(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height - 530,
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      SizedBox(
                        height: 100,
                        child: Image.asset('assets/dog.png'),
                      ),
                      SizedBox(
                        height: 100,
                        child: Image.asset('assets/dog.png'),
                      ),
                      SizedBox(
                        height: 100,
                        child: Image.asset('assets/dog.png'),
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      SizedBox(
                        height: 100,
                        child: Image.asset('assets/dog.png'),
                      ),
                      SizedBox(
                        height: 100,
                        child: Image.asset('assets/dog.png'),
                      ),
                      SizedBox(
                        height: 100,
                        child: Image.asset('assets/dog.png'),
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      SizedBox(
                        height: 100,
                        child: Image.asset('assets/dog.png'),
                      ),
                      SizedBox(
                        height: 100,
                        child: Image.asset('assets/dog.png'),
                      ),
                      SizedBox(
                        height: 100,
                        child: Image.asset('assets/dog.png'),
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      SizedBox(
                        height: 100,
                        child: Image.asset('assets/dog.png'),
                      ),
                      SizedBox(
                        height: 100,
                        child: Image.asset('assets/dog.png'),
                      ),
                      SizedBox(
                        height: 100,
                        child: Image.asset('assets/dog.png'),
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      SizedBox(
                        height: 100,
                        child: Image.asset('assets/dog.png'),
                      ),
                      SizedBox(
                        height: 100,
                        child: Image.asset('assets/dog.png'),
                      ),
                      SizedBox(
                        height: 100,
                        child: Image.asset('assets/dog.png'),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
