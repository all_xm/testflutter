import 'package:flutter/material.dart';

class ScrollPage extends StatelessWidget {
  const ScrollPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('스크롤 테스트'),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                children: [
                  Container(
                    color: Colors.greenAccent,
                    width: 100,
                    height: 100,
                  ),
                  Container(
                    color: Colors.blueAccent,
                    width: 100,
                    height: 100,
                  ),
                  Container(
                    color: Colors.amberAccent,
                    width: 100,
                    height: 100,
                  ),
                  Container(
                    color: Colors.pinkAccent,
                    width: 100,
                    height: 100,
                  ),
                  Container(
                    color: Colors.deepOrangeAccent,
                    width: 100,
                    height: 100,
                  ),
                ],
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            Container(
              color: Colors.indigoAccent,
              height: 1300,
            ),
          ],
        ),
      ),
    );
  }
}
