import 'package:first_project/components/component_mini_card.dart';
import 'package:flutter/material.dart';

class TestComponentPage extends StatefulWidget {
  const TestComponentPage({Key? key}) : super(key: key);

  @override
  State<TestComponentPage> createState() => _TestComponentPageState();
}

class _TestComponentPageState extends State<TestComponentPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              ComponentMiniCard(imageName: 'dog.png', imageText: '귀여움'),
              ComponentMiniCard(imageName: 'p1.jpeg', imageText: '펜션'),
              ComponentMiniCard(imageName: 'flour.jpg', imageText: '밀가루'),
            ],
          ),
        ],
      ),
    );
  }
}
