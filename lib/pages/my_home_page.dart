import 'package:flutter/material.dart';

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Column(
        mainAxisSize: MainAxisSize.max,
        children: [
          Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                color: Colors.blueAccent,
                width: MediaQuery.of(context).size.width,
                height: 100,
                child: Center(
                  child: Text(
                    '좌석배치도',
                    style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold, color: Colors.white),
                  ),
                ),
              )
            ],
          ),
          Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                width: MediaQuery.of(context).size.width / 4,
                height: 60,
                decoration: BoxDecoration(
                  border: Border.all(),
                  color: Colors.amberAccent
                ),
                child: Center(
                  child: Text('홍길동')
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width / 4,
                height: 60,
                decoration: BoxDecoration(
                    border: Border.all(),
                    color: Colors.amberAccent
                ),
                child: Center(
                    child: Text('가나다')
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width / 4,
                height: 60,
                decoration: BoxDecoration(
                    border: Border.all(),
                    color: Colors.amberAccent
                ),
                child: Center(
                    child: Text('김나나')
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width / 4,
                height: 60,
                decoration: BoxDecoration(
                    border: Border.all(),
                    color: Colors.amberAccent
                ),
                child: Center(
                    child: Text('꺄악')
                ),
              )
            ],
          ),
          Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                width: MediaQuery.of(context).size.width / 4,
                height: 60,
                decoration: BoxDecoration(
                    border: Border.all(),
                    color: Colors.amberAccent
                ),
                child: Center(
                    child: Text('홍길동')
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width / 4,
                height: 60,
                decoration: BoxDecoration(
                    border: Border.all(),
                    color: Colors.amberAccent
                ),
                child: Center(
                    child: Text('가나다')
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width / 4,
                height: 60,
                decoration: BoxDecoration(
                    border: Border.all(),
                    color: Colors.amberAccent
                ),
                child: Center(
                    child: Text('김나나')
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width / 4,
                height: 60,
                decoration: BoxDecoration(
                    border: Border.all(),
                    color: Colors.amberAccent
                ),
                child: Center(
                    child: Text('꺄악')
                ),
              )
            ],
          ),
          Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                width: MediaQuery.of(context).size.width / 4,
                height: 60,
                decoration: BoxDecoration(
                    border: Border.all(),
                    color: Colors.amberAccent
                ),
                child: Center(
                    child: Text('홍길동')
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width / 4,
                height: 60,
                decoration: BoxDecoration(
                    border: Border.all(),
                    color: Colors.amberAccent
                ),
                child: Center(
                    child: Text('가나다')
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width / 4,
                height: 60,
                decoration: BoxDecoration(
                    border: Border.all(),
                    color: Colors.amberAccent
                ),
                child: Center(
                    child: Text('김나나')
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width / 4,
                height: 60,
                decoration: BoxDecoration(
                    border: Border.all(),
                    color: Colors.amberAccent
                ),
                child: Center(
                    child: Text('꺄악')
                ),
              )
            ],
          ),
          Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                width: MediaQuery.of(context).size.width / 4,
                height: 60,
                decoration: BoxDecoration(
                    border: Border.all(),
                    color: Colors.amberAccent
                ),
                child: Center(
                    child: Text('홍길동')
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width / 4,
                height: 60,
                decoration: BoxDecoration(
                    border: Border.all(),
                    color: Colors.amberAccent
                ),
                child: Center(
                    child: Text('가나다')
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width / 4,
                height: 60,
                decoration: BoxDecoration(
                    border: Border.all(),
                    color: Colors.amberAccent
                ),
                child: Center(
                    child: Text('김나나')
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width / 4,
                height: 60,
                decoration: BoxDecoration(
                    border: Border.all(),
                    color: Colors.amberAccent
                ),
                child: Center(
                    child: Text('X')
                ),
              )
            ],
          )
        ],
      ),
    );
  }
}
